﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
    ],
  },
  {
    path: '/matches',
    name: 'Match',
    icon: 'CalendarOutlined',
    component: './Match',
  },
  {
    path: '/teams',
    name: 'Team',
    icon: 'SkinOutlined',
    component: './TableList',
  },
  {
    path: '/competitions',
    name: 'Competition',
    icon: 'TrophyOutlined',
    component: './TableList',
  },
  {
    path: '/users',
    name: 'User',
    icon: 'UserOutlined',
    component: './TableList',
  },
  {
    path: '/',
    redirect: '/matches',
  },
  {
    component: './404',
  },
];
