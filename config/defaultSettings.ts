import { Settings as LayoutSettings } from '@ant-design/pro-layout';

const Settings: LayoutSettings & {
  pwa?: boolean;
  logo?: string;
} = {
  navTheme: 'light',
  // 拂晓蓝
  primaryColor: '#1890ff',
  layout: 'mix',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  title: 'AA2888 Cambodia',
  pwa: false,
  logo: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.6435-9/156525901_3881078141959391_5763544112229320498_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=09cbfe&_nc_eui2=AeG4v2ll_K6dggQ2Dcopyanbsi2LhOnN--qyLYuE6c376lhrjZATjbW_nxlPAysSByaw9V5fe8FJQjLuyAnwEs5f&_nc_ohc=8l1bTqFgRmgAX8K7KJ0&_nc_oc=AQmmcLvhDJPREunDod47tIrcZ2RBWFHf9kBrPSor8Z2OEKR4WOuqNEcnaGw3rg2fcE0&_nc_ht=scontent.fpnh7-1.fna&oh=540b7077464dabc6e121a6461625a5f9&oe=60E51025',
  iconfontUrl: '',
};

export default Settings;
