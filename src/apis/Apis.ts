
import { extend, ResponseError } from "umi-request";

export interface errorType {
  body: any;
  bodyUsed: boolean;
  headers: any;
  ok: boolean;
  redirected: boolean;
  status: number;
  statusText: string;
  type: string;
  url: string;
  useCache: boolean;

}
export interface ResponseErroe {
  response: errorType;
  data: any;
  request: any;
  message: string;
}



const API = extend({
  prefix: 'http://api.aa2888cambodia.local', 
  params: {},
});
  
 
export const getAuthorizationHeader = () => {
  // const token = Cookies.get('token');
  const token ='';
  return {
    'Content-Type': 'application/json',
    // 'Authorization': `Bearer ${token}`,
  }
}

const codeMessage = {
  400: 'Bad Request.',
  401: 'Unauthorized',
  403: 'Forbidden.',
  404: 'Not Found',
  405: 'Method Not Allowed',
  406: 'Not Acceptable',
  408: 'Not Acceptable',
  410: 'Request Timeout',
  422: 'Unprocessable Entity',
  500: 'Internal Server Error',
  502: 'Bad Gateway',
  503: 'Service Unavailable',
  504: 'Gateway Timeout',
};

export const errorHandler = (error: ResponseError) => {

  const { response } = error;
  if (response && response.status) {
    const errorText = codeMessage[response.status] || response.statusText;
    if (response.status === 401) {
      // setTimeout(() => {
      //   localStorage.setItem('userInfo', '');
      //   if (window.location.pathname !== '/user/login') {
      //     history.replace({
      //       pathname: '/user/login',
      //       search: stringify({
      //         redirect: window.location.href,
      //       }),
      //     });
      //   }
      //   return ErrorMsg({ msg: `${status} ${errorText}` })
      // }, 1500);
    } else {
      // const { status, url } = response;
      return errorText;
    }

  }
  throw error;
};


export default API;