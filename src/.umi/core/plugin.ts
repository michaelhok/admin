// @ts-nocheck
import { Plugin } from 'D:/Hok/Businesses/Cam-Sports/Website/admin/node_modules/@umijs/runtime';

const plugin = new Plugin({
  validKeys: ['modifyClientRenderOpts','patchRoutes','rootContainer','render','onRouteChange','getInitialState','initialStateConfig','locale','layout','layoutActionRef','request',],
});

export { plugin };
