// @ts-nocheck

import CalendarOutlined from '@ant-design/icons/CalendarOutlined';
import SkinOutlined from '@ant-design/icons/SkinOutlined';
import TrophyOutlined from '@ant-design/icons/TrophyOutlined';
import UserOutlined from '@ant-design/icons/UserOutlined'

export default {
  CalendarOutlined,
SkinOutlined,
TrophyOutlined,
UserOutlined
}
    