import { useIntl } from 'umi';
import { DefaultFooter } from '@ant-design/pro-layout';

export default () => {
  const intl = useIntl();
  const defaultMessage = intl.formatMessage({
    id: 'app.copyright.produced',
    defaultMessage: '蚂蚁集团体验技术部出品',
  });

  return (
    <DefaultFooter
      copyright={`2021 ${defaultMessage}`}
      links={[
        {
          key: 'AA2888 Cambodia',
          title: 'AA2888 Cambodia',
          href: 'https://www.aa2888Cambodia.com',
          blankTarget: true,
        }
      ]}
    />
  );
};
