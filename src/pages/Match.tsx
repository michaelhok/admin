import React, { useEffect, useState } from 'react';
import {PageContainer} from "@ant-design/pro-layout";
import {Card, Table, Tag, Space ,Typography, Input, Button}  from 'antd'
import Login from '@/apis/login';
import request from 'umi-request';
import { updateWith } from 'lodash';
const Match: React.FC = ()=> {

  const {Text}=Typography

  const [matchList,setMatchList] = useState([]);
  const [isEdit,setIsEdit]= useState(false);
  const [num,setNum]= useState('');
  const [id,setID]= useState('');



  const columns = [
    {
      title: 'Number',
      dataIndex: 'number',
    },
    // {
    //   title: 'Competition',
    //   dataIndex: 'competition',
    //   render: text => <a>{text}</a>,
    // },
    // {
    //   title: 'Home Team',
    //   dataIndex: 'home_name',
    //   render: text => <a>{text}</a>,
    // },
    // {
    //   title: 'Away Team',
    //   dataIndex: 'away_team',
    //   render: text => <a>{text}</a>,
    // },
    // {
    //   title: 'Date/Time',
    //   dataIndex: 'date_time',
    // },
    {
      title: 'Action',
      key: 'action',
      render: (text,record) => (
        <Space size="middle">
          <a onClick={()=>{
            setIsEdit(true);
            setNum(record.number);
            setID(record.id)
          }}>Edit</a>
          <a>Delete</a>
        </Space>
      ),
    },
  ];

 
  

  useEffect(()=>{
    getmatch()
  },[])

  const getmatch=()=>{
    Login.match().then((res)=>{
      const {status,code,data} = res;
      if(status===1){
        const{items}=data
        setMatchList(items)

      }else{

      }
      console.log('jsonmovei',JSON.stringify(res))

    }).catch((err)=>{

    })
  }

  const renderEdit=()=>(
    <Card>
      <Input
        value={num}
        onChange={(e)=>{
          const {value} = e.target
          setNum(value)
          console.log(e)      
        }}   
      />

      <Button onClick={()=>{
        submitData(id)
      }}>
        submit
      </Button>

    </Card>
  )

  const submitData=(id:number)=>{
    const data={
      'id':id,
      'name':num
    }
    Login.udatematch(data).then((res)=>{
      getmatch()
    })

  }
  return(
    <PageContainer>
      <Card title="Match">
        {
          isEdit?(
            renderEdit()
          ): <Table bordered columns={columns} dataSource={matchList} />
         
        }
        </Card> 
    </PageContainer>

  )
}

export default  Match;
